<?php
/**
 * Created by PhpStorm.
 * User: woutv
 * Date: 13-4-2018
 * Time: 10:12
 */

require_once("conn.php");

class TaskManager {
    use Conn;

    private $conn;

    public function __construct() {

        $this->conn = $this->makeConnection();
    }

    //
    public function getTasks() {
        $queryResult = $this->conn->query("SELECT taakid, title, content FROM taken ORDER BY postdate");

        if ($queryResult->rowCount() > 0) {

            $result = "<html></body>";

            while ($row = $queryResult->fetch()) {
                $result = $result . "
                <style>
                    table {
                    border-collapse:collapse;
                    width: 80%;
                    margin-left: 10%;
                    margin-bottom: 50px;
                    padding: 5px;
                    }

                    textarea {
                    width:100%;
                    }
                    
                    .titel {
                    text-align: center;
                    }
                    
                </style>
                <form method='POST' action=". $this->updateTask($row["taakid"]) .">
                    <table>
                        <tr>
                            <td class='titel'>
                            <label>Titel</label><br/>
                                <input type='text' name='title' value='" . $row["title"] . "'/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <label>Taak</label>
                                <textarea rows='5' name='content'>" . $row["content"] . "</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                    <input type='submit' name='add". $row["taakid"] ."' value='Update taak' />
                </form>
                    <form method='POST' action=". $this->deleteTask($row["taakid"]) .">
                        <input type='submit' name='del". $row["taakid"] ."' value='Verwijder taak' />
                    </form>
                        </td>
                    </tr>
                </tbody></table>
                ";
            }

            $result = $result . "</body></html>";
            return $result;
        }
        return "Check je code";
    }

    public function addTask() {
        if (isset($_POST['submit'])) {

            $title = $_POST['title'];
            $content = str_replace('
				', '<br/>', $_POST['content']);

            $this->conn->query("INSERT INTO taken (title, content) VALUES ('" . $title . "', '" . $content . "')");
        }
    }

    public function updateTask($id) {

        if (isset($_POST['add'. $id])) {

            $title = $_POST['title'];
            $content = $_POST['content'];
            
            $this->conn->query("UPDATE taken SET title='". $title ."', content='". $content ."'  WHERE taakid= ". $id);
            header("Refresh:0");
        }
    }

    public function deleteTask($id) {

        if (isset($_POST['del'. $id])) {

            $this->conn->query("DELETE FROM taken WHERE taakid = ". $id);
            header("Refresh:0");
        }
    }
}


