<html>
<body>
<style>
    html {
        background-color: aquamarine;
    }
    table {
        margin-top: 15%;
        margin-left: 30%;
        width: 40%;
        border-collapse: collapse;
    }
    td {
        padding: 25px 20px;
        text-align: center;
    }

    a {
        padding: 20px 10px;
        color: white;
        background-color: slategrey;
        border-radius: 20px;
    }
</style>
<table>
    <tr><td><a href="./1_no_class_no_pdo"/>1. No class no PDO</a></td></tr>
    <tr><td><a href="./2_no_class_pdo"/>2. No class using PDO</a></td></tr>
    <tr><td><a href="./3_class_pdo"/>3. Both using a class as PDO</a></td></tr>
    <tr><td><a href="http://localhost:8000/"/>4. Laravel application</td></tr>
    <tr><td><a href="./cms-blog/">CMS blog</a></td></tr>
</table>
</body>
</html>
