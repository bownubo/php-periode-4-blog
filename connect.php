<?php
/**
 * Created by PhpStorm.
 * User: Stan v.d Berg
 * Date: 6/8/2018
 * Time: 9:06 AM
 */

function connection() {
	$host = 'localhost';
	$database = 'phpblog';
	$user = 'root';
	$password = '';

	return mysqli_connect($host, $user, $password, $database);
}
if(mysqli_connect_errno()) {
	die('connection failed'. mysqli_connect_error(). "(". mysqli_connect_errno().")");
}

function PDOconnection() {
	try {
		$pdo = new PDO("mysql:host=localhost;dbname=phpblog;", 'root', '');
		return $pdo;

	}	catch(PDOException $e){
		echo "Gekke Error:".$e->getMessage()."<br>";
	}
}

trait PDOClassConnection {
	public function conn() {
		try {
			$pdo = new PDO("mysql:host=localhost;dbname=phpblog;", 'root', '');
			return $pdo;

		}	catch(PDOException $e){
			echo "Gekke Error:".$e->getMessage()."<br>";
		}
	}
}