<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Taken extends Migration
{

    public function up()
    {
        Schema::create('taken', function (Blueprint $table){
					$table->increments('taakid');
					$table->string('title');
					$table->text('content');
					$table->timestamps('timestamp');
        });
		}


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
