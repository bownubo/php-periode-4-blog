<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', 'Formcontroller@addtask');
Route::get('/', 'Formcontroller@index');
Route::post('/delete', 'Formcontroller@deletetask');
Route::post('/update', 'Formcontroller@updatetask');