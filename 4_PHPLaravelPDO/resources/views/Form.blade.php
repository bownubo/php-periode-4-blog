<!doctype html>
<style>
    table {
        border-collapse:collapse;
        width: 80%;
        margin-left: 10%;
        margin-bottom: 50px;
        padding: 5px;
    }

    textarea {
        width:100%;
    }

    .titel {
        text-align: center;
    }
</style>
<form action="./" method="POST">
    @csrf
    <label>Titel</label>
    <br/>
    <input type="text" name="title">
    <br/>
    <label>Taak</label><br>
    <textarea rows='4' cols='80' name='content'></textarea>
    <br/>
    <input type='submit' name='submit' value='Voeg toe'/>
</form>

@foreach( $tasks as $row)
    <table>
        <form method='POST' action="./update">
            @csrf
            <tbody>
            <tr>
                <td class="titel">
                    <label>Titel</label><br/>
                    <input type='text' name='title' value='{{$row["title"]}}'/>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Taak</label>
                    <textarea rows='5' name='content'>{{$row["content"]}}</textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" value="{{$row["taakid"]}}" name="id" />
                    <input type='submit' name='update' value='Update taak' />
        </form>
        <form method='POST' action="./delete">
            @csrf
            <input type="hidden" value="{{$row["taakid"]}}" name="id" />
            <input type='submit' name='delete' value='Verwijder taak' />
        </form>
        </td>
        </tr>
        </tbody>
    </table>
@endforeach
NEW MESSAGES
