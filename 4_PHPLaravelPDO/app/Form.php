<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Form extends Model
{
	protected $table = "taken";
	public $timestamps = false;

	protected function scopeGetTasks(){
		$queryGet = DB::table('taken')
			->get()
			->toArray();
			$queryGet = json_decode(json_encode($queryGet), true);

			return $queryGet;

	}

	protected function scopeInsertTasks($request){
		$title = $request->input('title');
		$content = $request->input('content');
		$queryInsert = DB::table('taken')
			->insert(['title' => $title, 'content' => $content]);
		return Form::scopeGetTasks();
	}

	protected function scopeDeleteTasks($request){
		$id = $request->input('id');
		DB::table('taken')
			->where('taakid',  $id)
			->delete();
			return Form::scopeGetTasks();
	}

	protected function scopeUpdateTasks($request) {
		$title = $request->input('title');
		$content = $request->input('content');
		$id = $request->input('id');
		DB::table('taken')
			->where('taakid', $id)
			->update(['title' => $title, 'content' => $content]);
			return Form::scopeGetTasks();
	}
}

