<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;

class Formcontroller extends Controller
{
	public function index()
	{
		$results = Form::scopeGetTasks();

		return view('Form', ['tasks' => $results]);
	}
	public function addtask(Request $request){
		$results = Form::scopeInsertTasks($request);
		return view('Form', ['tasks' => $results]);

	}

	public function deletetask(Request $request){
		$results = Form::scopeDeleteTasks($request);

		return view('Form', ['tasks' => $results]);
	}

	public function updatetask(Request $request){
		$results = Form::scopeUpdateTasks($request);
		return view('Form', ['tasks' => $results]);
	}



}