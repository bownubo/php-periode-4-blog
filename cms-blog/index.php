<?php session_start() ?>

<html>
<style>
    body{
        background-color: grey;
    }

    .loginform p {
        height:8%;
        color: #FFF;
        text-shadow: 1px 1px #FF0000;
    }

    .loginform {
        width:250px;
        height: auto;
        margin: 10% auto auto;
    }
    .loginform input {
        height: 40px;
        width: 100%;
        margin: auto;
        padding: 6px 12px 6px 12px;
    }

    .loginform .button {
        width: 100%;
        border-radius: 7px;
        background: deepskyblue;
        border: none;
        color: white;
        margin-top: 10px;
        outline: none;
        font-size: 13px;
        border-bottom: 3px solid darkblue;
        cursor: pointer;
    }
</style>

<head>

    <title>Blog - Login</title>

</head>

<body>

    <form class="loginform" action='logincheck.php' method='POST'>
        <p><?php if (isset($_SESSION['error'])){
                echo $_SESSION['error'];
            }?></p>

        <input type="text" name="username" placeholder="username" />
        <input type="password" name="password" placeholder="password" />
        <input class="button" type="submit" name="submit" value="Sign in"/>
    </form>

</body>
</html>