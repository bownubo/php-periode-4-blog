<?php

require_once 'conn.php';

function getTasks($pageid) {
    $conn = makeConnection();
    $queryResult = $conn->query("SELECT taakid, title, content FROM taken WHERE userid = '". $_SESSION['userid'] ."' AND pageid = $pageid ORDER BY postdate");

    if ($queryResult->rowCount() > 0) {

        $result = "<html></body>";

        while ($row = $queryResult->fetch()) {
            $result = $result . "
                <style>
                    table {
                    border-collapse:collapse;
                    width: 80%;
                    margin-left: 10%;
                    margin-bottom: 50px;
                    padding: 5px;
                    }

                    textarea {
                    width:100%;
                    }
                    
                    .titel {
                    text-align: center;
                    }
                </style>
                <form method='POST' action=". updateTask($row["taakid"]) .">
                    <table>
                        <tr>
                            <td class='titel'>
                                <label>Titel</label><br/>
                                <input type='text' name='title' value='" . $row["title"] . "'/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Taak</label>
                                <textarea style=\"max-width: 1200px; max-height:300px\" rows='5' name='content'>" . $row["content"] . "</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                    <input type='submit' name='add". $row["taakid"] ."' value='Update taak' />
                </form>
                    <form method='POST' action=". deleteTask($row["taakid"]) .">
                        <input type='submit' name='del". $row["taakid"] ."' value='Verwijder taak' />
                    </form>
                        </td>
                    </tr>
                </tbody></table>
                ";
        }

        $result = $result . "</body></html>";
        return $result;
    }
    return "Voeg taken toe om deze in te zien!";
}

function addTask($pageid) {
    if (isset($_POST['submit'])) {

        $title = $_POST['title'];
        $content = str_replace('
				', '<br/>', $_POST['content']);

        $conn = makeConnection();
        $conn->query("INSERT INTO taken (pageid, userid, title, content) VALUES ('" . $pageid . "', '" . $_SESSION['userid'] . "' , '" . $title . "' , '" . $content . "')");
    }
}

function updateTask($id) {

    if (isset($_POST['add'. $id])) {

        $title = $_POST['title'];
        $content = $_POST['content'];

        $conn = makeConnection();
        $conn->query("UPDATE taken SET title='". $title ."', content='". $content ."'  WHERE taakid= ". $id);
        header("Refresh:0");
    }
}

function deleteTask($id) {

    if (isset($_POST['del'. $id])) {

        $conn = makeConnection();
        $conn->query("DELETE FROM taken WHERE taakid = ". $id);
        header("Refresh:0");
    }
}