<?php
function makeConnection() {
    try {
        $pdo = new PDO("mysql:host=localhost;dbname=phpblog;", 'root', '');
        return $pdo;
    } catch (PDOException $e) {
        $error = "Unexpected error:" . $e->getMessage() . "<br>";
        echo $error;
        return;
    }
}