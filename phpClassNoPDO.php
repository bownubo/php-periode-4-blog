<?php
/**
 * Created by PhpStorm.
 * User: woutv
 * Date: 13-4-2018
 * Time: 10:12
 */

trait Connection {
    public function conn() {
        $host = 'localhost';
        $database = 'phpblog';
        $user = 'root';
        $password = '';

        if (mysqli_connect_errno()) {
            die('connection failed'. mysqli_connect_error(). "(". mysqli_connect_errno().")");
        }

        return mysqli_connect($host, $user, $password, $database);
    }
}

class TaskManager {
    use Connection;

    private $query;
    private $connection;

    public function __construct() {
        $this->setConnection();


    }

    public function getTasks() {
        $result = $this->getConnection()->query("SELECT title, content FROM taken ORDER BY postdate");

        if ($result->num_rows > 0) {

            echo "<html></body><table style='border: solid black 1px; border-collapse:collapse; text-align: left; width: 100%;'>";
            while ($row = $result->fetch_assoc()) {
                echo "
                <tr><td style='border-bottom: 1px solid black;'><b>" . $row["title"] . "</b></td><td style='border-bottom: 1px solid black'>" . $row["content"] . "</td></tr>
            ";
            }
        }

        echo "</tbody></table></body></html> ";
    }

    public function addTask() {
        echo "<html><body>";

        echo "
    
    <form action='#' method='POST'>
        Titel:<br/></titel>
        <input type='text' name='title' />
        <br/>
        Taak:<br/>
        <textarea rows='4' cols='80' name='content'></textarea>
        <br/>
        <input type='submit' name='submit'/>
    </form>
    
    ";
        echo "</body></html>";

        if (isset($_POST['title'])) {

            $content = str_replace('
', '<br/>', $_POST['content']);

            mysqli_query($this->getConnection(), "INSERT INTO taken (title, content) VALUES ('" . $_POST['title'] . "', '" . $content . "')");
        }
    }

    public function setConnection() {
        $host = 'localhost';
        $database = 'phpblog';
        $user = 'root';
        $password = '';

        if (mysqli_connect_errno()) {
            die('connection failed'. mysqli_connect_error(). "(". mysqli_connect_errno().")");
        }

        $this->connection = mysqli_connect($host, $user, $password, $database);
    }

    public function getConnection() {
        return $this->connection;
    }

}

$task = new TaskManager();

$task->addTask();

$task->getTasks();
